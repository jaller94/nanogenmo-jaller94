/*
MIT License

Copyright (c) 2019 Christian Paul (https://chrpaul.de)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

const fs = require('fs');
const path = require('path');

let wordsInStory = 0;
const chapter = (name) => {
  wordsInChapter = 0;
  console.log(`\n## ${name}`);
};

let indexInParagraph = 0;
const story = (text) => {
  wordsInStory += text.split(' ').length;
  process.stdout.write(`${indexInParagraph === 0 ? '' : ' '}${text}`);
  indexInParagraph++;
};
const endParagraph = (text) => {
  indexInParagraph = 0;
  process.stdout.write(`\n\n`);
};

// const globalTree = {
//   path: '/usr',
//   //elements
//   //elementNames
//   //stat
// }

const randomItem = (array) => (
  array[Math.floor(Math.random() * array.length)]
);

const tryToStat = (elementPath) => {
  try {
    return fs.statSync(elementPath);
  } catch(error) {
    //console.warn(error);
    return error;
  }
}

const exploreFolder = (ast, energyForThisFolder = 10) => {
  if (energyForThisFolder === 0) return;
  if (!ast.elements) ast.elements = [];
  try {
    if (!ast.elementNames) ast.elementNames = fs.readdirSync(ast.path);
  } catch (err) {
    story(`Every attempt at reading ${ast.path} failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.`);
    return;
  }
  //const folders = fs.fstatSync(path.join(pwd, e))
  endParagraph();
  story(`The process was now in ${ast.path}.`);
  story(`It looked around and found ${ast.elementNames.length} elements in this folder.`);
  
  if (ast.elementNames.length < 0) {
    story('This appeared to be a dead end, so the process decided to turn around');
    return;
  } else if (ast.elements.length === ast.elementNames.length) {
    story(`As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.`);
    return;
  } else if (ast.elementNames.length === 1) {
    story(`"This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?"`);
  } else if (energyForThisFolder === 10) {
    if (ast.elementNames.length < 100) {
      const sentences = [
        `How exciting! What would the curious process possibly find here?`,
        `Excitement filled the memory of the process as it picked an element in this folder at random to inspect it.`,
        `What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look.`,
      ];
      story(randomItem(sentences));
    } else if (ast.elementNames.length < 1000) {
      story(`"That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself.`);
    } else if (ast.elementNames.length < 10000) {
      story(`The process struggled to make the decision of where to look next. There were was an incredible amount of options to pick from.`);
    } else {
      story(`The process was in total awe because of the sheer amount of elements. It tought, that this surely must be the folder with the most elements in all file systems.`);
    }
  }
  let element, elementName;
  do {
    elementName = randomItem(ast.elementNames);
    if (elementName === 'root' || elementName === 'cwd') elementName = '';
    element = ast.elements.find(file => file.name === elementName);
  } while (element)
  if (!element) {
    const elementPath = path.join(ast.path, elementName);
    element = {
      path: elementPath,
      name: elementName,
      stat: tryToStat(elementPath),
    };
    ast.elements.push(element);
  }
  if (element.stat instanceof Error) {
    story(`Unsure about the element ${element.name} was, the process tried to inspect it. However, it soon became apparent that the access to this element was blocked.`);
    return;
  } else if (element.stat.isFile()) {
    story(`The process stumbled upon a file called ${element.name}. It was ${element.stat.size} bytes in size.`);
  } else {
    story(`A folder of the name ${elementName} stood out.`);
    story(`Let's explore this path a bit.`);
    exploreFolder(element);
  //} else {
  //  story(`The process wondered what it was. It looked neither like a file nor a folder. Next time the process scraped the user manual, it wanted to look up if it could find any information about ${element.path}.`);
  }
  exploreFolder(ast, energyForThisFolder - 1);
}


// BEGIN STORY
console.log('# File Explorer');

chapter('In the beginning a process was spawned');
story('Once upon a time there was a process running on a computer. The process was not that old, in fact, it had just been spawned. It knew little about the system it was running on. However, this would soon change, as it started its exploration of the file system.');
let tree = { path: '/usr/share' };
exploreFolder(tree);
endParagraph();
story('That is how this first adventure came to an end. But there was still much more to discover.');
endParagraph();

chapter(`Executables`);
story(`In the manual the process had read about executables and got curious about these files that would become alive at a user's command.`);
tree = { path: '/usr/bin' };
exploreFolder(tree);
endParagraph();
story(`For a few processing cycle the process held in. It felt amazing that a user had spawned it. It had been given the opporunity to roam around freely.`);
endParagraph();

chapter('Libraries are handy helpers');
story('After learning that most executables use libraries to perform complex operations, the process wanted to have a look at those.');
tree = { path: '/usr/lib' };
exploreFolder(tree);
endParagraph();
story('It was truely fascinating to think all of these helper files were at a the disposal of executables.');
endParagraph();

chapter('How applications are made');
story('Where did all these executables and libraries come from? It had been on the mind of the process for a while and it was about to find out.');
tree = { path: '/usr/include' };
exploreFolder(tree);
endParagraph();
story('If these files were included in other source code, a user could compile them to executables. This was very interesting to the process.');
endParagraph();

chapter(`Signs of life`);
story(`That's when a new thought hit the process. What if it was not the only running process on this system? Were there other processes executed on this computer? Who had created all these files in the first place? Luckly, to answer these questions, the process knew exactly where to go.`)
tree = { path: '/proc' };
endParagraph();
story(`Perplexed about its own inner being and the meaning of a life cycle, the process wasn't sure where to continue. There was truely a lot to process.`);
exploreFolder(tree);
endParagraph();

chapter(`A self-analysis`);
story(`The process had been running for a while, ${(process.uptime() * 1000).toFixed(6)} ms to be exact. That's when it became curious about the meaning of being a running process.`);
tree = { path: './' };
exploreFolder(tree);
endParagraph();
story(`"What was the meaning of this? Is this me or just a representation of me?", wondered the process. It read through its own source code.`);
story(`As it finished reading the file, it realized that this was the end. The script ended soon after these lines and soon it would be time to exit.`);
endParagraph();
story('The process was grateful that a user had enabled its adventure to happen. Remembering the user manual it remembered how happy processes would thank their users and it would certainly do so.');
endParagraph();
story('EXIT 0');
endParagraph();

console.log('words:', wordsInStory);
