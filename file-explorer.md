# File Explorer

## In the beginning a process was spawned
Once upon a time there was a process running on a computer. The process was not that old, in fact, it had just been spawned. It knew little about the system it was running on. However, this would soon change, as it started its exploration of the file system.

The process was now in /usr/share. It looked around and found 293 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. A folder of the name evolution stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called filtertypes.xml. It was 776286 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name images stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called paper.png. It was 266 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called draft-stamp.jpg. It was 4660 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called texture.png. It was 137 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called draft-paper.png. It was 597 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called working.png. It was 5689 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called world_map-960.png. It was 111584 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called confidential-stamp.jpg. It was 5708 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called rect.png. It was 3795 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called ribbon.jpg. It was 3193 bytes in size.

The process was now in /usr/share/evolution/images. It looked around and found 10 elements in this folder. The process stumbled upon a file called midnight-stars.jpg. It was 7190 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name views stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name addressbook stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views/addressbook. It looked around and found 4 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called Phone_List.galview. It was 283 bytes in size.

The process was now in /usr/share/evolution/views/addressbook. It looked around and found 4 elements in this folder. The process stumbled upon a file called galview.xml. It was 384 bytes in size.

The process was now in /usr/share/evolution/views/addressbook. It looked around and found 4 elements in this folder. The process stumbled upon a file called By_Company.galview. It was 302 bytes in size.

The process was now in /usr/share/evolution/views/addressbook. It looked around and found 4 elements in this folder. The process stumbled upon a file called Address_Cards.galview. It was 65 bytes in size.

The process was now in /usr/share/evolution/views/addressbook. It looked around and found 4 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. A folder of the name calendar stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views/calendar. It looked around and found 2 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called galview.xml. It was 751 bytes in size.

The process was now in /usr/share/evolution/views/calendar. It looked around and found 2 elements in this folder. The process stumbled upon a file called List_View.galview. It was 236 bytes in size.

The process was now in /usr/share/evolution/views/calendar. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. A folder of the name tasks stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views/tasks. It looked around and found 4 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called galview.xml. It was 361 bytes in size.

The process was now in /usr/share/evolution/views/tasks. It looked around and found 4 elements in this folder. The process stumbled upon a file called With_DueDate.galview. It was 182 bytes in size.

The process was now in /usr/share/evolution/views/tasks. It looked around and found 4 elements in this folder. The process stumbled upon a file called With_Status.galview. It was 182 bytes in size.

The process was now in /usr/share/evolution/views/tasks. It looked around and found 4 elements in this folder. The process stumbled upon a file called Tasks.galview. It was 159 bytes in size.

The process was now in /usr/share/evolution/views/tasks. It looked around and found 4 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. A folder of the name memos stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views/memos. It looked around and found 2 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called galview.xml. It was 163 bytes in size.

The process was now in /usr/share/evolution/views/memos. It looked around and found 2 elements in this folder. The process stumbled upon a file called Memos.galview. It was 169 bytes in size.

The process was now in /usr/share/evolution/views/memos. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. A folder of the name mail stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called Wide_View_Normal.galview. It was 169 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called As_Sent_Folder.galview. It was 228 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called Wide_View_Sent.galview. It was 169 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called By_Sender.galview. It was 281 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called Messages.galview. It was 228 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called By_Follow_Up_Flag.galview. It was 331 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called By_Status.galview. It was 281 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called galview.xml. It was 903 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. The process stumbled upon a file called By_Subject.galview. It was 281 bytes in size.

The process was now in /usr/share/evolution/views/mail. It looked around and found 9 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution/views. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. The process stumbled upon a file called address_formats.dat. It was 5180 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name errors stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called addressbook.error. It was 7061 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called mail.error. It was 32365 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called widgets.error. It was 1520 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called shell.error. It was 5411 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called calendar.error. It was 26370 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called evolution-offline-alert.error. It was 500 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called org-gnome-itip-formatter.error. It was 693 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called filter.error. It was 1595 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called org-gnome-external-editor.error. It was 803 bytes in size.

The process was now in /usr/share/evolution/errors. It looked around and found 16 elements in this folder. The process stumbled upon a file called org-gnome-attachment-reminder.error. It was 622 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name etspec stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called e-meeting-time-sel.etspec. It was 1656 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. The process stumbled upon a file called e-task-table.etspec. It was 2475 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. The process stumbled upon a file called e-addressbook-view.etspec. It was 6640 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. The process stumbled upon a file called e-cal-list-view.etspec. It was 1955 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. The process stumbled upon a file called e-memo-table.etspec. It was 1478 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. The process stumbled upon a file called message-list.etspec. It was 4282 bytes in size.

The process was now in /usr/share/evolution/etspec. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name ui stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called mail-dialogs.ui. It was 20831 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called e-html-editor-manager.ui. It was 6201 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called fullname.ui. It was 13478 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called e-calendar-preferences.ui. It was 90285 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called evolution-memos.ui. It was 2946 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called org-gnome-email-custom-header.ui. It was 5653 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called evolution-tasks.ui. It was 3439 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called smime-ui.ui. It was 39541 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called e-send-options.ui. It was 54644 bytes in size.

The process was now in /usr/share/evolution/ui. It looked around and found 24 elements in this folder. The process stumbled upon a file called contact-editor.ui. It was 152994 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. The process stumbled upon a file called vfoldertypes.xml. It was 680473 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. The process stumbled upon a file called searchtypes.xml. It was 701508 bytes in size.

The process was now in /usr/share/evolution. It looked around and found 19 elements in this folder. A folder of the name ecps stood out. Let's explore this path a bit.

The process was now in /usr/share/evolution/ecps. It looked around and found 3 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called phonelist.ecps. It was 923 bytes in size.

The process was now in /usr/share/evolution/ecps. It looked around and found 3 elements in this folder. The process stumbled upon a file called smallbook.ecps. It was 896 bytes in size.

The process was now in /usr/share/evolution/ecps. It looked around and found 3 elements in this folder. The process stumbled upon a file called medbook.ecps. It was 918 bytes in size.

The process was now in /usr/share/evolution/ecps. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name glib-2.0 stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name valgrind stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/valgrind. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called glib.supp. It was 15067 bytes in size.

The process was now in /usr/share/glib-2.0/valgrind. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. A folder of the name gettext stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/gettext. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name po stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/gettext/po. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called Makefile.in.in. It was 8076 bytes in size.

The process was now in /usr/share/glib-2.0/gettext/po. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/glib-2.0/gettext. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. A folder of the name gdb stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/gdb. It looked around and found 2 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called gobject_gdb.py. It was 10205 bytes in size.

The process was now in /usr/share/glib-2.0/gdb. It looked around and found 2 elements in this folder. The process stumbled upon a file called glib_gdb.py. It was 8069 bytes in size.

The process was now in /usr/share/glib-2.0/gdb. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. A folder of the name schemas stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called org.gnucash.dialogs.import.ofx.gschema.xml. It was 448 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.evolution.calendar.gschema.xml. It was 23622 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.FileRoller.gschema.xml. It was 8509 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.freedesktop.ColorHelper.gschema.xml. It was 1499 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.Evolution.DefaultSources.gschema.xml. It was 1053 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.builder.gnome-code-assistance.gschema.xml. It was 471 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.desktop.privacy.gschema.xml. It was 3789 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.builder.plugins.color_picker_plugin.gschema.xml. It was 2124 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called 99_manjaro-xfce.gschema.override. It was 60 bytes in size.

The process was now in /usr/share/glib-2.0/schemas. It looked around and found 130 elements in this folder. The process stumbled upon a file called org.gnome.builder.workbench.gschema.xml. It was 1482 bytes in size.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. A folder of the name codegen stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name __pycache__ stood out. Let's explore this path a bit.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called utils.cpython-37.opt-1.pyc. It was 3295 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called __init__.cpython-37.pyc. It was 313 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called codegen_docbook.cpython-37.opt-1.pyc. It was 11994 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called parser.cpython-37.pyc. It was 5310 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called codegen_main.cpython-37.opt-1.pyc. It was 6767 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called __init__.cpython-37.opt-1.pyc. It was 313 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called parser.cpython-37.opt-1.pyc. It was 5310 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called codegen_main.cpython-37.pyc. It was 6767 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called codegen.cpython-37.pyc. It was 117092 bytes in size.

The process was now in /usr/share/glib-2.0/codegen/__pycache__. It looked around and found 16 elements in this folder. The process stumbled upon a file called codegen_docbook.cpython-37.pyc. It was 11994 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called codegen.py. It was 221901 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called parser.py. It was 12715 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called codegen_docbook.py. It was 16253 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called codegen_main.py. It was 12454 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called config.py. It was 819 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called dbustypes.py. It was 19568 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called utils.py. It was 4003 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. The process stumbled upon a file called __init__.py. It was 984 bytes in size.

The process was now in /usr/share/glib-2.0/codegen. It looked around and found 9 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/glib-2.0. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name ImageMagick-7 stood out. Let's explore this path a bit.

The process was now in /usr/share/ImageMagick-7. It looked around and found 3 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called english.xml. It was 51826 bytes in size.

The process was now in /usr/share/ImageMagick-7. It looked around and found 3 elements in this folder. The process stumbled upon a file called locale.xml. It was 2461 bytes in size.

The process was now in /usr/share/ImageMagick-7. It looked around and found 3 elements in this folder. The process stumbled upon a file called francais.xml. It was 54794 bytes in size.

The process was now in /usr/share/ImageMagick-7. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name systemtap stood out. Let's explore this path a bit.

The process was now in /usr/share/systemtap. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name tapset stood out. Let's explore this path a bit.

The process was now in /usr/share/systemtap/tapset. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called node.stp. It was 3495 bytes in size.

The process was now in /usr/share/systemtap/tapset. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/systemtap. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name groff stood out. Let's explore this path a bit.

The process was now in /usr/share/groff. It looked around and found 3 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name site-tmac stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/site-tmac. It looked around and found 2 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called man.local. It was 483 bytes in size.

The process was now in /usr/share/groff/site-tmac. It looked around and found 2 elements in this folder. The process stumbled upon a file called mdoc.local. It was 477 bytes in size.

The process was now in /usr/share/groff/site-tmac. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff. It looked around and found 3 elements in this folder. A folder of the name 1.22.4 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name font stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name devpdf stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called NI. It was 16784 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called TR. It was 8933 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called AI. It was 12789 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called BMB. It was 10388 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called download. It was 2590 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called ABI. It was 12978 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called Foundry. It was 4863 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called ZD. It was 5474 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called EURO. It was 1127 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devpdf. It looked around and found 41 elements in this folder. The process stumbled upon a file called HR. It was 18880 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devlbp stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called TR. It was 14234 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HB. It was 13998 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called TB. It was 14226 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called DESC. It was 170 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called CI. It was 7033 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HNB. It was 13658 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called TI. It was 14289 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HNBI. It was 13666 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called EB. It was 7031 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HI. It was 14001 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devX75-12 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called S. It was 3206 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NBI. It was 2818 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HB. It was 2818 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NI. It was 2791 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CR. It was 2786 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CB. It was 2785 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HR. It was 2816 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called DESC. It was 116 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NB. It was 2814 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HI. It was 2810 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devhtml stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called CR. It was 25107 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CBI. It was 25093 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CB. It was 25092 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called R. It was 25091 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called BI. It was 25107 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called I. It was 25106 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CI. It was 25092 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called B. It was 25106 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called S. It was 25091 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called DESC. It was 220 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devcp1047 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called BI. It was 3661 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called I. It was 3660 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 88 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called B. It was 3660 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called R. It was 3645 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devcp1047. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devX100 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called TI. It was 2938 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CR. It was 2836 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called NBI. It was 2973 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CI. It was 2834 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called HR. It was 2928 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CBI. It was 2842 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called HBI. It was 2940 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CB. It was 2838 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called DESC. It was 117 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called S. It was 3305 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devlj4 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called DESC. It was 146 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called TNRI. It was 19263 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called ALBR. It was 19287 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. A folder of the name generate stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called Makefile. It was 7458 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. The process stumbled upon a file called wingdings.map. It was 5774 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. The process stumbled upon a file called special.awk. It was 2680 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. The process stumbled upon a file called text.map. It was 16238 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. The process stumbled upon a file called special.map. It was 4235 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. The process stumbled upon a file called symbol.map. It was 7535 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4/generate. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called ABI. It was 19116 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called UCBI. It was 22536 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called AI. It was 19001 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called TNRB. It was 15143 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called LGR. It was 15100 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called TNRBI. It was 19164 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devX100-12 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called CBI. It was 3091 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HR. It was 2972 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NI. It was 2993 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called DESC. It was 117 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HI. It was 2969 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NR. It was 2983 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HBI. It was 2994 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CB. It was 3092 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CR. It was 3089 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called TBI. It was 2976 bytes in size.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devlatin1 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called R. It was 3636 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. The process stumbled upon a file called B. It was 3651 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. The process stumbled upon a file called I. It was 3651 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 88 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. The process stumbled upon a file called BI. It was 3652 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devlatin1. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4/font. It looked around and found 14 elements in this folder. A folder of the name devascii stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called I. It was 1930 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called R. It was 1915 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 88 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called BI. It was 1931 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called B. It was 1930 bytes in size.

The process was now in /usr/share/groff/1.22.4/font/devascii. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. The process stumbled upon a file called eign. It was 620 bytes in size.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. A folder of the name pic stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/pic. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called chem.pic. It was 3031 bytes in size.

The process was now in /usr/share/groff/1.22.4/pic. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. A folder of the name oldfont stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/oldfont. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name devps stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called HNB. It was 7917 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called HNR. It was 8199 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called NI. It was 9591 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called HB. It was 7969 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called CR. It was 6760 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called NBI. It was 10019 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called symbol.afm. It was 9739 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called SS. It was 7446 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called symbolsl.afm. It was 9407 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called PB. It was 7056 bytes in size.

The process was now in /usr/share/groff/1.22.4/oldfont. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. A folder of the name tmac stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called mm.tmac. It was 49 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called hdmisc.tmac. It was 3425 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called hyphen.sv. It was 34834 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called refer-me.tmac. It was 2850 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called me.tmac. It was 53 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called mdoc.tmac. It was 57 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. A folder of the name mdoc stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/1.22.4/tmac/mdoc. It looked around and found 4 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called doc-common. It was 34717 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac/mdoc. It looked around and found 4 elements in this folder. The process stumbled upon a file called doc-ditroff. It was 5777 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac/mdoc. It looked around and found 4 elements in this folder. The process stumbled upon a file called doc-syms. It was 25516 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac/mdoc. It looked around and found 4 elements in this folder. The process stumbled upon a file called doc-nroff. It was 4522 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac/mdoc. It looked around and found 4 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called hyphen.fr. It was 28215 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called papersize.tmac. It was 4332 bytes in size.

The process was now in /usr/share/groff/1.22.4/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called pspic.tmac. It was 4998 bytes in size.

The process was now in /usr/share/groff/1.22.4. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff. It looked around and found 3 elements in this folder. A folder of the name current stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name tmac stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called devtag.tmac. It was 3830 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called europs.tmac. It was 1689 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called unicode.tmac. It was 93 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called refer-mm.tmac. It was 2946 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called dvi.tmac. It was 24533 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called latin9.tmac. It was 2272 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called sv.tmac. It was 4403 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called www.tmac. It was 40923 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called mm.tmac. It was 49 bytes in size.

The process was now in /usr/share/groff/current/tmac. It looked around and found 81 elements in this folder. The process stumbled upon a file called Xps.tmac. It was 1783 bytes in size.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. A folder of the name pic stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/pic. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called chem.pic. It was 3031 bytes in size.

The process was now in /usr/share/groff/current/pic. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. A folder of the name font stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name devX100-12 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called HR. It was 2972 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NB. It was 3029 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CI. It was 3090 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called S. It was 3354 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HBI. It was 2994 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called DESC. It was 117 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CBI. It was 3091 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CR. It was 3089 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HB. It was 2998 bytes in size.

The process was now in /usr/share/groff/current/font/devX100-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called TR. It was 2956 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devlj4 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called TBI. It was 25788 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called S. It was 13464 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called LGB. It was 15105 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called ALBR. It was 19287 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called TNRBI. It was 19164 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called AI. It was 19001 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called TNRI. It was 19263 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called GI. It was 22561 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called LGI. It was 19059 bytes in size.

The process was now in /usr/share/groff/current/font/devlj4. It looked around and found 48 elements in this folder. The process stumbled upon a file called ABI. It was 19116 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devX75-12 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called TB. It was 2793 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called TR. It was 2781 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called TI. It was 2790 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HR. It was 2816 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NI. It was 2791 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called NR. It was 2800 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called S. It was 3206 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called CR. It was 2786 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HI. It was 2810 bytes in size.

The process was now in /usr/share/groff/current/font/devX75-12. It looked around and found 18 elements in this folder. The process stumbled upon a file called HB. It was 2818 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devlbp stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called CR. It was 7033 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HR. It was 13981 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called EB. It was 7031 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called EI. It was 7031 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called TBI. It was 14071 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HB. It was 13998 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called HNR. It was 13653 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called ER. It was 7031 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called TR. It was 14234 bytes in size.

The process was now in /usr/share/groff/current/font/devlbp. It looked around and found 19 elements in this folder. The process stumbled upon a file called CI. It was 7033 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devdvi stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called HITC. It was 4433 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called SC. It was 3567 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called CWEC. It was 6956 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called TBIEC. It was 50090 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called TBI. It was 7381 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called TBEC. It was 37632 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called S. It was 4073 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called HBIEC. It was 35511 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called TRTC. It was 3877 bytes in size.

The process was now in /usr/share/groff/current/font/devdvi. It looked around and found 38 elements in this folder. The process stumbled upon a file called SA. It was 4793 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devcp1047 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called BI. It was 3661 bytes in size.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 88 bytes in size.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called B. It was 3660 bytes in size.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called I. It was 3660 bytes in size.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. The process stumbled upon a file called R. It was 3645 bytes in size.

The process was now in /usr/share/groff/current/font/devcp1047. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devutf8 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called B. It was 25057 bytes in size.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. The process stumbled upon a file called R. It was 25042 bytes in size.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. The process stumbled upon a file called BI. It was 25058 bytes in size.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. The process stumbled upon a file called I. It was 25057 bytes in size.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 96 bytes in size.

The process was now in /usr/share/groff/current/font/devutf8. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devX100 stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called CBI. It was 2842 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called HBI. It was 2940 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called HI. It was 2941 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called DESC. It was 117 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called TB. It was 2945 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called S. It was 3305 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CB. It was 2838 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called TBI. It was 2940 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called CI. It was 2834 bytes in size.

The process was now in /usr/share/groff/current/font/devX100. It looked around and found 18 elements in this folder. The process stumbled upon a file called NR. It was 2949 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devhtml stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called CBI. It was 25093 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called B. It was 25106 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called R. It was 25091 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called DESC. It was 220 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CB. It was 25092 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called I. It was 25106 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CR. It was 25107 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called S. It was 25091 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called BI. It was 25107 bytes in size.

The process was now in /usr/share/groff/current/font/devhtml. It looked around and found 10 elements in this folder. The process stumbled upon a file called CI. It was 25092 bytes in size.

The process was now in /usr/share/groff/current/font. It looked around and found 14 elements in this folder. A folder of the name devascii stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called BI. It was 1931 bytes in size.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called DESC. It was 88 bytes in size.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called I. It was 1930 bytes in size.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called R. It was 1915 bytes in size.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. The process stumbled upon a file called B. It was 1930 bytes in size.

The process was now in /usr/share/groff/current/font/devascii. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. The process stumbled upon a file called eign. It was 620 bytes in size.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. A folder of the name oldfont stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/oldfont. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name devps stood out. Let's explore this path a bit.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called CBI. It was 9432 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called TB. It was 7739 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called symbolsl.afm. It was 9407 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called PB. It was 7056 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called symbol.afm. It was 9739 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called S. It was 5779 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called zapfdr.ps. It was 4962 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called PBI. It was 9072 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called NR. It was 7652 bytes in size.

The process was now in /usr/share/groff/current/oldfont/devps. It looked around and found 30 elements in this folder. The process stumbled upon a file called HR. It was 8233 bytes in size.

The process was now in /usr/share/groff/current/oldfont. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff/current. It looked around and found 5 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/groff. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name graphite2 stood out. Let's explore this path a bit.

The process was now in /usr/share/graphite2. It looked around and found 2 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called graphite2.cmake. It was 3069 bytes in size.

The process was now in /usr/share/graphite2. It looked around and found 2 elements in this folder. The process stumbled upon a file called graphite2-release.cmake. It was 894 bytes in size.

The process was now in /usr/share/graphite2. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name gnome-games stood out. Let's explore this path a bit.

The process was now in /usr/share/gnome-games. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name options stood out. Let's explore this path a bit.

The process was now in /usr/share/gnome-games/options. It looked around and found 2 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called desmume.options. It was 37 bytes in size.

The process was now in /usr/share/gnome-games/options. It looked around and found 2 elements in this folder. The process stumbled upon a file called desmume2015.options. It was 37 bytes in size.

The process was now in /usr/share/gnome-games/options. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/gnome-games. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name color stood out. Let's explore this path a bit.

The process was now in /usr/share/color. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name icc stood out. Let's explore this path a bit.

The process was now in /usr/share/color/icc. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name colord stood out. Let's explore this path a bit.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called FOGRA29L_uncoated.icc. It was 122260 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called DonRGB4.icc. It was 10608 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called EktaSpacePS5.icc. It was 12316 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called CIE-RGB.icc. It was 10880 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called ProPhotoRGB.icc. It was 16756 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called GRACoL_TR006_coated.icc. It was 122056 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called ECI-RGBv2.icc. It was 12792 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called PAL-RGB.icc. It was 11392 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called ColorMatchRGB.icc. It was 15324 bytes in size.

The process was now in /usr/share/color/icc/colord. It looked around and found 38 elements in this folder. The process stumbled upon a file called Bluish.icc. It was 14764 bytes in size.

The process was now in /usr/share/color/icc. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/color. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name clementine stood out. Let's explore this path a bit.

The process was now in /usr/share/clementine. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name projectm-presets stood out. Let's explore this path a bit.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called Geiss - Demonic Distortion.milk. It was 1705 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Idiot - What Shall Come.milk. It was 3641 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Aderrasi - Antidote.milk. It was 2091 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Zylot - Tangent Universe (Collapsed With Artifact Mix).milk. It was 1568 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Idiot - Cortex (Spiritual Visions Mix).milk. It was 2795 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Aderrasi - Ashes Of Air (Remix).milk. It was 1492 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Rovastar - The Chaos Of Colours.milk. It was 6697 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Geiss - Runoff.milk. It was 1323 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Geiss - Dynamic Swirls 2.milk. It was 1980 bytes in size.

The process was now in /usr/share/clementine/projectm-presets. It looked around and found 571 elements in this folder. The process stumbled upon a file called Telek - Slow Shift Matrix (bb4.5).milk. It was 1467 bytes in size.

The process was now in /usr/share/clementine. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share. It looked around and found 293 elements in this folder. A folder of the name terminfo stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name t stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called terminator. It was 1632 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called tvi92B. It was 629 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called ti916-220-8. It was 1448 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called trsII. It was 451 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called ti700. It was 396 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called tty4424m. It was 486 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called ti924-8. It was 585 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called tvi920b-vb-unk. It was 1160 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called ti926. It was 597 bytes in size.

The process was now in /usr/share/terminfo/t. It looked around and found 256 elements in this folder. The process stumbled upon a file called ts100. It was 1159 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name v stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called vt320-k311. It was 948 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vt220d. It was 1537 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vanilla. It was 336 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vip-H. It was 1299 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vc303. It was 309 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vk100. It was 615 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vt125. It was 1186 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vt420pc. It was 2073 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vc415. It was 409 bytes in size.

The process was now in /usr/share/terminfo/v. It looked around and found 146 elements in this folder. The process stumbled upon a file called vte-2012. It was 3213 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name A stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/A. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called Apple_Terminal. It was 2473 bytes in size.

The process was now in /usr/share/terminfo/A. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name i stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called ibm5151. It was 1251 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm3151. It was 1286 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called icl6404-w. It was 713 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm6154-c. It was 604 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm8513. It was 1905 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibcs2. It was 1020 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called intext2. It was 644 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm8512. It was 1905 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm+16color. It was 1098 bytes in size.

The process was now in /usr/share/terminfo/i. It looked around and found 85 elements in this folder. The process stumbled upon a file called ibm6153-40. It was 1229 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name b stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called blit. It was 490 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bq300-pc. It was 1596 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called beehiveIIIm. It was 422 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bq300-8w. It was 1543 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called b-128. It was 1032 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bobcat. It was 516 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bq300-rv. It was 1636 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bee. It was 509 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bq300. It was 1646 bytes in size.

The process was now in /usr/share/terminfo/b. It looked around and found 50 elements in this folder. The process stumbled upon a file called bct510a. It was 1181 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name c stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called cons60-iso. It was 1518 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called cons25. It was 1502 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called cons25r-m. It was 1524 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called cons50l1. It was 1522 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called contel320. It was 560 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called cons50-iso-m. It was 1270 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called citoh. It was 431 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called cops-10. It was 370 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called c100-rv. It was 826 bytes in size.

The process was now in /usr/share/terminfo/c. It looked around and found 125 elements in this folder. The process stumbled upon a file called concept108. It was 950 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name a stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. The process stumbled upon a file called aj832. It was 357 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called att5425-nl. It was 1542 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called avt-w. It was 1226 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called ansi-color-2-emx. It was 1716 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called ansi-nt. It was 476 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called ansi80x25-mono. It was 1478 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called aaa-db. It was 1268 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called att4425. It was 1514 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called aaa-60. It was 1237 bytes in size.

The process was now in /usr/share/terminfo/a. It looked around and found 322 elements in this folder. The process stumbled upon a file called aaa. It was 1289 bytes in size.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name 9 stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/9. It looked around and found 3 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called 955-w. It was 773 bytes in size.

The process was now in /usr/share/terminfo/9. It looked around and found 3 elements in this folder. The process stumbled upon a file called 955-hb. It was 775 bytes in size.

The process was now in /usr/share/terminfo/9. It looked around and found 3 elements in this folder. The process stumbled upon a file called 9term. It was 850 bytes in size.

The process was now in /usr/share/terminfo/9. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name E stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/E. It looked around and found 4 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called Eterm-256color. It was 2514 bytes in size.

The process was now in /usr/share/terminfo/E. It looked around and found 4 elements in this folder. The process stumbled upon a file called Eterm-88color. It was 2444 bytes in size.

The process was now in /usr/share/terminfo/E. It looked around and found 4 elements in this folder. The process stumbled upon a file called Eterm-color. It was 2274 bytes in size.

The process was now in /usr/share/terminfo/E. It looked around and found 4 elements in this folder. The process stumbled upon a file called Eterm. It was 2274 bytes in size.

The process was now in /usr/share/terminfo/E. It looked around and found 4 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/share/terminfo. It looked around and found 42 elements in this folder. A folder of the name z stood out. Let's explore this path a bit.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called z340-nam. It was 1585 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called zen30. It was 446 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called ztx-1-a. It was 535 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z110. It was 707 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z100. It was 707 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z8001. It was 527 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z29. It was 1219 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z-100. It was 707 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called ztx. It was 535 bytes in size.

The process was now in /usr/share/terminfo/z. It looked around and found 32 elements in this folder. The process stumbled upon a file called z29a-nkc-uc. It was 1433 bytes in size.

That is how this first adventure came to an end. But there was still much more to discover.


## Executables
In the manual the process had read about executables and got curious about these files that would become alive at a user's command.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process struggled to make the decision of where to look next. There were was an incredible amount of options to pick from. The process stumbled upon a file called noise. It was 22648 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called line-xor. It was 14088 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called ip6tables-nft-save. It was 220208 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called vbo. It was 22288 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called gtkdoc-scangobj. It was 3287 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called sg_map. It was 18208 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called sg_write_verify. It was 22800 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called dcadec. It was 31112 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called pronto2lirc. It was 6507 bytes in size.

The process was now in /usr/bin. It looked around and found 3963 elements in this folder. The process stumbled upon a file called bluetooth. It was 1178 bytes in size.

For a few processing cycle the process held in. It felt amazing that a user had spawned it. It had been given the opporunity to roam around freely.


## Libraries are handy helpers
After learning that most executables use libraries to perform complex operations, the process wanted to have a look at those.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process struggled to make the decision of where to look next. There were was an incredible amount of options to pick from. The process stumbled upon a file called libpotrace.so. It was 34408 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libnumbertext-1.0.so.0. It was 356168 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libytnef.so.0.0.0. It was 97400 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libv4l2rds.so. It was 30384 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libfreehand-0.1.so.1. It was 440104 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libmlt++.so.6.18.0. It was 157536 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libzmq.a. It was 2152050 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libwmf-0.2.so.7. It was 340632 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libdrm_nouveau.so.2.0.0. It was 34408 bytes in size.

The process was now in /usr/lib. It looked around and found 4723 elements in this folder. The process stumbled upon a file called libgsturidownloader-1.0.so.0. It was 34496 bytes in size.

It was truely fascinating to think all of these helper files were at a the disposal of executables.


## How applications are made
Where did all these executables and libraries come from? It had been on the mind of the process for a while and it was about to find out.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. The process struggled to make the decision of where to look next. There were was an incredible amount of options to pick from. The process stumbled upon a file called brlapi_keycodes.h. It was 6854 bytes in size.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name cdio++ stood out. Let's explore this path a bit.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called enum.hpp. It was 1392 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called track.hpp. It was 3152 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called cdtext.hpp. It was 3092 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called iso9660.hpp. It was 11943 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called cdio.hpp. It was 4056 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called devices.hpp. It was 5988 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called disc.hpp. It was 4248 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called read.hpp. It was 3861 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called mmc.hpp. It was 13531 bytes in size.

The process was now in /usr/include/cdio++. It looked around and found 10 elements in this folder. The process stumbled upon a file called device.hpp. It was 6064 bytes in size.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /usr/include/attr. It looked around and found 3 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called attributes.h. It was 8010 bytes in size.

The process was now in /usr/include/attr. It looked around and found 3 elements in this folder. The process stumbled upon a file called error_context.h. It was 1569 bytes in size.

The process was now in /usr/include/attr. It looked around and found 3 elements in this folder. The process stumbled upon a file called libattr.h. It was 1411 bytes in size.

The process was now in /usr/include/attr. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. The process stumbled upon a file called libunwind-common.h. It was 11545 bytes in size.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name epoxy stood out. Let's explore this path a bit.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called glx_generated.h. It was 66492 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called egl.h. It was 1840 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called gl_generated.h. It was 1466041 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called common.h. It was 1990 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called glx.h. It was 1920 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called egl_generated.h. It was 87509 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. The process stumbled upon a file called gl.h. It was 3104 bytes in size.

The process was now in /usr/include/epoxy. It looked around and found 7 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. The process stumbled upon a file called ladspa.h. It was 27593 bytes in size.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. The process stumbled upon a file called dbstl_element_ref.h. It was 23501 bytes in size.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name libswscale stood out. Let's explore this path a bit.

The process was now in /usr/include/libswscale. It looked around and found 2 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called version.h. It was 1927 bytes in size.

The process was now in /usr/include/libswscale. It looked around and found 2 elements in this folder. The process stumbled upon a file called swscale.h. It was 12015 bytes in size.

The process was now in /usr/include/libswscale. It looked around and found 2 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name enchant-2 stood out. Let's explore this path a bit.

The process was now in /usr/include/enchant-2. It looked around and found 3 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called enchant-provider.h. It was 4817 bytes in size.

The process was now in /usr/include/enchant-2. It looked around and found 3 elements in this folder. The process stumbled upon a file called enchant++.h. It was 7215 bytes in size.

The process was now in /usr/include/enchant-2. It looked around and found 3 elements in this folder. The process stumbled upon a file called enchant.h. It was 13020 bytes in size.

The process was now in /usr/include/enchant-2. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /usr/include. It looked around and found 1269 elements in this folder. A folder of the name bellagio stood out. Let's explore this path a bit.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called OMXComponentRMExt.h. It was 1889 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called tsemaphore.h. It was 2705 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_comp_debug_levels.h. It was 2515 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_base_video_port.h. It was 3122 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called st_static_component_loader.h. It was 4818 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_base_sink.h. It was 2806 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_base_port.h. It was 14682 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_reference_resource_manager.h. It was 2552 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_classmagic.h. It was 2710 bytes in size.

The process was now in /usr/include/bellagio. It looked around and found 23 elements in this folder. The process stumbled upon a file called omx_base_filter.h. It was 3229 bytes in size.

If these files were included in other source code, a user could compile them to executables. This was very interesting to the process.


## Signs of life
That's when a new thought hit the process. What if it was not the only running process on this system? Were there other processes executed on this computer? Who had created all these files in the first place? Luckly, to answer these questions, the process knew exactly where to go.

Perplexed about its own inner being and the meaning of a life cycle, the process wasn't sure where to continue. There was truely a lot to process.

The process was now in /proc. It looked around and found 289 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. A folder of the name 508 stood out. Let's explore this path a bit.

The process was now in /proc/508. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called mem. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/508/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/508. It looked around and found 51 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 24069 stood out. Let's explore this path a bit.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. A folder of the name task stood out. Let's explore this path a bit.

The process was now in /proc/24069/task. It looked around and found 4 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 24079 stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24079. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/24069/task. It looked around and found 4 elements in this folder. A folder of the name 24072 stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name 7 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/7 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 16 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/16 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 15 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/15 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 19 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/19 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 17 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/17 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 14 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/14 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. A folder of the name net stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called sockstat. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called softnet_stat. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip_mr_vif. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called igmp6. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called nf_conntrack_expect. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called fib_triestat. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called udplite6. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called wireless. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called anycast6. It was 0 bytes in size.

The process was now in /proc/24069/task/24072/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called dev_mcast. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called gid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name 14 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/14 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 19 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/19 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 16 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/16 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. The process stumbled upon a file called 2. It was 167014 bytes in size.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 17 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/17 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 10 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/10 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072/fd. It looked around and found 20 elements in this folder. A folder of the name 15 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24072/fd/15 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called exe. It was 14088 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/24069/task/24072. It looked around and found 45 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/24069/task. It looked around and found 4 elements in this folder. A folder of the name 24069 stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called 18. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 8. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 7. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 12. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 16. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 15. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 3. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 4. It was 0 bytes in size.

The process was now in /proc/24069/task/24069/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 17. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24069. It looked around and found 45 elements in this folder. The process stumbled upon a file called mem. It was 0 bytes in size.

The process was now in /proc/24069/task. It looked around and found 4 elements in this folder. A folder of the name 24071 stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called 2. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 10. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 18. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 6. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 4. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 5. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 13. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 12. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/fdinfo. It looked around and found 20 elements in this folder. The process stumbled upon a file called 11. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 14 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/14 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 18 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/18 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 12 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/12 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 13 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/13 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 10 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/10 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 16 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/16 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 15 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/15 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 7 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/7 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/24069/task/24071/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 7 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/7 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 12 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/12 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 16 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/16 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 15 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/15 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 10 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/10 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 8 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/8 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. The process stumbled upon a file called 2. It was 167014 bytes in size.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 14 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/14 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 13 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/13 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 17 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/17 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071/fd. It looked around and found 20 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/task/24071/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called exe. It was 14088 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/24069/task/24071. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/24069/task. It looked around and found 4 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountstats. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called coredump_filter. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountstats. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called timerslack_ns. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. A folder of the name net stood out. Let's explore this path a bit.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called nf_conntrack_expect. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called dev_mcast. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called udp. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. A folder of the name netfilter stood out. Let's explore this path a bit.

The process was now in /proc/24069/net/netfilter. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called nf_log. It was 0 bytes in size.

The process was now in /proc/24069/net/netfilter. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called igmp6. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip6_flowlabel. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip6_mr_vif. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called tcp. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip_mr_cache. It was 0 bytes in size.

The process was now in /proc/24069/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called rt6_stats. It was 0 bytes in size.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name 10 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/10 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 9 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/9 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 12 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/12 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 8 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/8 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 16 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/16 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 11 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/11 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069/fd. It looked around and found 20 elements in this folder. A folder of the name 7 stood out. Let's explore this path a bit. Every attempt at reading /proc/24069/fd/7 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/24069. It looked around and found 51 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 884 stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called timerslack_ns. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/884/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called pagemap. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called autogroup. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called timers. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 32768 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 11 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/11 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 2 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/2 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 1990656 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 4239512 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountstats. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name task stood out. Let's explore this path a bit.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name 884 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called 7. It was 1990656 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 4239512 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 2 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/2 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 32768 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called 5. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 6. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 0. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 2. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 3. It was 0 bytes in size.

The process was now in /proc/884/task/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 11. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. A folder of the name 890 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. A folder of the name 892 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_score_adj. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name map_files stood out. Let's explore this path a bit.

The process was now in /proc/884/map_files. It looked around and found 117 elements in this folder. "That's quite a lot of elements. I hope I can find something of interest.", thought the process to itself. Unsure about the element 7f918fda8000-7f918fda9000 was, the process tried to inspect it. However, it soon became apparent that the access to this element was blocked.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name net stood out. Let's explore this path a bit.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called fib_triestat. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip_mr_cache. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called xfrm_stat. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called raw6. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called udp. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called igmp. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called psched. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called ip_tables_names. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called dev_mcast. It was 0 bytes in size.

The process was now in /proc/884/net. It looked around and found 54 elements in this folder. A folder of the name netfilter stood out. Let's explore this path a bit.

The process was now in /proc/884/net/netfilter. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" The process stumbled upon a file called nf_log. It was 0 bytes in size.

The process was now in /proc/884/net/netfilter. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called timerslack_ns. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountstats. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called pagemap. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called 5. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 2. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 0. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 6. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 4. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 3. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 1990656 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 4239512 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 11 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/11 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 32768 bytes in size.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/fd. It looked around and found 11 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called coredump_filter. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_score_adj. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called pagemap. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mem. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called 4. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 11. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 0. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 6. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 5. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 0 bytes in size.

The process was now in /proc/884/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 3. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called gid_map. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_score_adj. It was 0 bytes in size.

The process was now in /proc/884. It looked around and found 51 elements in this folder. A folder of the name task stood out. Let's explore this path a bit.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 892 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/884/task/892/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/task/892/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884/task/892. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. A folder of the name 890 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called 8. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 5. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 0. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 11. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 2. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 1. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 3. It was 0 bytes in size.

The process was now in /proc/884/task/890/fdinfo. It looked around and found 11 elements in this folder. The process stumbled upon a file called 6. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/884/task/890/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/884/task/890. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 2 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/2 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 1990656 bytes in size.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 11 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/11 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 32768 bytes in size.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/890/fd. It looked around and found 11 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/890/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. A folder of the name 884 stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 7. It was 1990656 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 9. It was 32768 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 2 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/2 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. The process stumbled upon a file called 8. It was 4239512 bytes in size.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884/fd. It looked around and found 11 elements in this folder. A folder of the name 11 stood out. Let's explore this path a bit. Every attempt at reading /proc/884/task/884/fd/11 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called gid_map. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/884/task/884/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called mem. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called exe. It was 214952 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/884/task/884. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/884/task. It looked around and found 3 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/884. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 16789 stood out. Let's explore this path a bit.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit. Every attempt at reading /proc/16789/ns failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit. Every attempt at reading /proc/16789/fd failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/16789. It looked around and found 51 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 475 stood out. Let's explore this path a bit.

The process was now in /proc/475. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called timerslack_ns. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit. Every attempt at reading /proc/475/fdinfo failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/475. It looked around and found 51 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. The process stumbled upon a file called misc. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. The process stumbled upon a file called cgroups. It was 0 bytes in size.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 556 stood out. Let's explore this path a bit.

The process was now in /proc/556. It looked around and found 51 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called pagemap. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called mounts. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. A folder of the name map_files stood out. Let's explore this path a bit. Every attempt at reading /proc/556/map_files failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called timers. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/556. It looked around and found 51 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit. Every attempt at reading /proc/556/fdinfo failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 10 stood out. Let's explore this path a bit.

The process was now in /proc/10. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name task stood out. Let's explore this path a bit.

The process was now in /proc/10/task. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name 10 stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit. Every attempt at reading /proc/10/task/10/fdinfo failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called loginuid. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name fdinfo stood out. Let's explore this path a bit. Every attempt at reading /proc/10/task/10/fdinfo failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/10/task/10/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called gid_map. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called children. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called schedstat. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. A folder of the name net stood out. Let's explore this path a bit.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called udplite. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called dev_mcast. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called snmp. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called netstat. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called arp. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called mcfilter. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called nf_conntrack. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called nf_conntrack_expect. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called anycast6. It was 0 bytes in size.

The process was now in /proc/10/task/10/net. It looked around and found 54 elements in this folder. The process stumbled upon a file called psched. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called smaps. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called io. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/10/task/10. It looked around and found 45 elements in this folder. The process stumbled upon a file called clear_refs. It was 0 bytes in size.

The process was now in /proc/10/task. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/10. It looked around and found 51 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/10. It looked around and found 51 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/10. It looked around and found 51 elements in this folder. The process stumbled upon a file called gid_map. It was 0 bytes in size.

The process was now in /proc/10. It looked around and found 51 elements in this folder. The process stumbled upon a file called timerslack_ns. It was 0 bytes in size.

The process was now in /proc/10. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/10. It looked around and found 51 elements in this folder. Unsure about the element exe was, the process tried to inspect it. However, it soon became apparent that the access to this element was blocked.

The process was now in /proc. It looked around and found 289 elements in this folder. A folder of the name 764 stood out. Let's explore this path a bit.

The process was now in /proc/764. It looked around and found 51 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called limits. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/764. It looked around and found 51 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called latency. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/764. It looked around and found 51 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called exe. It was 410240 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_score. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name  stood out. Let's explore this path a bit.

The process was now in /proc/764. It looked around and found 51 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. What first appeared to be an ordinary folder would surely be a treasure trove, if one was willing to look. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/764/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountstats. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called wchan. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called status. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called auxv. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called numa_maps. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called stat. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called stack. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called personality. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called smaps_rollup. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called maps. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called mountinfo. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called pagemap. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name task stood out. Let's explore this path a bit.

The process was now in /proc/764/task. It looked around and found 1 elements in this folder. "This surely must have been be an important element.", the process exclaimed. "Why else would it get its own folder to reside in?" A folder of the name 764 stood out. Let's explore this path a bit.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called statm. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called sched. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. A folder of the name fd stood out. Let's explore this path a bit.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. A folder of the name 5 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/5 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 3 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/3 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 1 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/1 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 8 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/8 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 2 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/2 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 0 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/0 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 4 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/4 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 6 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/6 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. A folder of the name 7 stood out. Let's explore this path a bit. Every attempt at reading /proc/764/task/764/fd/7 failed. The process wondered what mysterious secrets where hidden here but there was no point in moving on.

The process was now in /proc/764/task/764/fd. It looked around and found 9 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called cpuset. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called sessionid. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called projid_map. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. A folder of the name attr stood out. Let's explore this path a bit.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called exec. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called prev. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called sockcreate. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called current. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called fscreate. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. The process stumbled upon a file called keycreate. It was 0 bytes in size.

The process was now in /proc/764/task/764/attr. It looked around and found 6 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called cmdline. It was 0 bytes in size.

The process was now in /proc/764/task/764. It looked around and found 45 elements in this folder. The process stumbled upon a file called syscall. It was 0 bytes in size.

The process was now in /proc/764/task. It looked around and found 1 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called oom_adj. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called environ. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called coredump_filter. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. A folder of the name ns stood out. Let's explore this path a bit.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. Excitement filled the memory of the process as it picked an element in this folder at random to inspect it. The process stumbled upon a file called uts. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called user. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called cgroup. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called ipc. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called net. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called mnt. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. The process stumbled upon a file called pid_for_children. It was 0 bytes in size.

The process was now in /proc/764/ns. It looked around and found 8 elements in this folder. As the process had already given all elements in this folder a try, it seemed the right time to look somewhere else.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called uid_map. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called comm. It was 0 bytes in size.

The process was now in /proc/764. It looked around and found 51 elements in this folder. The process stumbled upon a file called setgroups. It was 0 bytes in size.


## A self-analysis
The process had been running for a while, 247.812666 ms to be exact. That's when it became curious about the meaning of being a running process.

The process was now in ./. It looked around and found 2 elements in this folder. The process stumbled upon a file called fairly-ok-version.md. It was 369602 bytes in size.

The process was now in ./. It looked around and found 2 elements in this folder. How exciting! What would the curious process possibly find here? The process stumbled upon a file called index.js. It was 8523 bytes in size.

"What was the meaning of this? Is this me or just a representation of me?", wondered the process. It read through its own source code. As it finished reading the file, it realized that this was the end. The script ended soon after these lines and soon it would be time to exit.

The process was grateful that a user had enabled its adventure to happen. Remembering the user manual it remembered how happy processes would thank their users and it would certainly do so.

EXIT 0
